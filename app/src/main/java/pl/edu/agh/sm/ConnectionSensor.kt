package pl.edu.agh.sm

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.telephony.TelephonyManager

class ConnectionSensor(private val context: Context) {
    var type: String = "?"

    fun refreshInfo() {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val nc = cm.getNetworkCapabilities(cm.activeNetwork)
        if(nc==null) {
            type = "None"
            return
        }

        if (nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
            type = "Wi-Fi"
        } else if (nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
            type = when (cm.activeNetworkInfo.subtype) {
                TelephonyManager.NETWORK_TYPE_EDGE -> "EDGE"
                TelephonyManager.NETWORK_TYPE_HSPA -> "HSPA"
                TelephonyManager.NETWORK_TYPE_HSPAP -> "HSPAP"
                TelephonyManager.NETWORK_TYPE_LTE -> "LTE"
                else -> "?"
            }
        } else {
            type = "None"
        }
    }
}
