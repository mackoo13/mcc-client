package pl.edu.agh.sm.data

data class TaskSample(var remote: Boolean?,
                      var deviceModel: String,
                      var taskType: Int,
                      var taskSize: Double,
                      var isCharging: Boolean,
                      var batteryLevel: Double,
                      var connectionType: String,
                      var yearDay: Int,
                      var weekDay: Int,
                      var currentTime: Int,
                      var time: Double,
                      var timeRange: String?
)