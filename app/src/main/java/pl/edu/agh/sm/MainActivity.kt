@file:Suppress("UsePropertyAccessSyntax")

package pl.edu.agh.sm

import android.os.*
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pl.edu.agh.sm.data.TaskSample
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var taskService: TaskService
    private lateinit var batterySensor: BatterySensor
    private lateinit var connectionSensor: ConnectionSensor
    private var fileName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        taskService = TaskService()
        batterySensor = BatterySensor(applicationContext)
        connectionSensor = ConnectionSensor(applicationContext)

        setListeners()
        setupSpinner()
    }

    private val messageHandler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(inputMessage: Message) {
            Toast.makeText(this@MainActivity, inputMessage.obj as String, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setListeners() {
        val randomStuffButton: Button = findViewById(R.id.randomStuffButton)

        randomStuffButton.setOnClickListener {
            GlobalScope.launch {
                runTask(fileName)
            }
        }

        val batchButton: Button = findViewById(R.id.batchButton)

        batchButton.setOnClickListener {

            GlobalScope.launch {
                for (i in 0..11) {
                    val res = "xd"
                    val name = "%02d.mp4".format(7-(i / 2 + 1))
                    val fileName = res + name
                    runTask(fileName)
                }
            }
        }
    }

    private fun runTask(fileName: String) {
        val now = Calendar.getInstance()

        batterySensor.refreshInfo()
        val batteryLevel = batterySensor.batteryPct
        val isCharging = batterySensor.isCharging

        connectionSensor.refreshInfo()
        val connectionType = connectionSensor.type

        if (batteryLevel != null && isCharging != null) {
            val taskSample = TaskSample(
                null,
                Build.MODEL,
                0,
                0.0,
                isCharging,
                batteryLevel.toDouble(),
                connectionType,
                now.get(Calendar.DAY_OF_YEAR),
                now.get(Calendar.DAY_OF_WEEK),
                now.get(Calendar.HOUR) * 60 + now.get(Calendar.MINUTE),
                -1.0,
                null
            )
            taskService.convert(fileName, taskSample, messageHandler)
            messageHandler.obtainMessage(0, "Converted $fileName").sendToTarget()
        } else println("Cannot read battery data")
    }

    private fun setupSpinner() {
        val fileNameSpinner: Spinner = findViewById(R.id.fileNameSpinner)

        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item,
            taskService.availableFiles()
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        fileNameSpinner.setAdapter(adapter)
        fileNameSpinner.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View,
                position: Int, id: Long
            ) {
                fileName = parent.getItemAtPosition(position) as String
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }
}
