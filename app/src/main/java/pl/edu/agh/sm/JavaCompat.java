package pl.edu.agh.sm;

import org.jcodec.common.Tuple;

public class JavaCompat {

    public static <T0, T1, T2> Tuple._3<T0, T1, T2> tuple3(T0 v0, T1 v1, T2 v2) {
        return new Tuple._3<>(v0, v1, v2);
    }

}
