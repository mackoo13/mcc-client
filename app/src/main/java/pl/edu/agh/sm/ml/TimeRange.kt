package pl.edu.agh.sm.ml

enum class TimeRange {
    V_SHORT,
    SHORT,
    MEDIUM,
    LONG,
    V_LONG
}