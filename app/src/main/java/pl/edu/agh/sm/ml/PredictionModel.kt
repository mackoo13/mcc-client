package pl.edu.agh.sm.ml

import pl.edu.agh.sm.data.TaskSample

class PredictionModel(private val wekaConfig: WekaConfig) {
    fun predict(taskSample: TaskSample): TimeRange? {
        wekaConfig.loadModel()

        if(wekaConfig.regressor == null) return null

        wekaConfig.addTestTaskSample(taskSample)
        val prediction = wekaConfig.regressor?.classifyInstance(wekaConfig.testData.lastInstance()) ?: return null
        return TimeRange.values()[prediction.toInt()]
    }

    companion object {
        fun discretise(time: Double?): TimeRange? {
            if(time == null) return null
            if(time < 80000) return TimeRange.V_SHORT
            if(time < 100000) return TimeRange.SHORT
            if(time < 140000) return TimeRange.MEDIUM
            if(time < 200000) return TimeRange.LONG
            return TimeRange.V_LONG
        }
    }
}