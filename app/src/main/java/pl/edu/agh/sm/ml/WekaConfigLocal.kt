package pl.edu.agh.sm.ml

import android.os.Build
import pl.edu.agh.sm.data.TaskSample
import weka.classifiers.trees.J48
import weka.core.*
import java.io.File
import java.io.FileInputStream

class WekaConfigLocal(serverPath: String, modelDir: String, modelType: String, deviceType: String?) :
    WekaConfig(serverPath, modelDir, modelType, deviceType) {

    val attrs = ArrayList<Attribute>(arrayListOf(attrTaskType, attrTaskSize,
        attrClass))

    override val trainingData = Instances("train", attrs, 0)
    override val testData = Instances("test", attrs, 0)
    override val modelPath = "$modelDir/time_${Build.MODEL}.weka"

    init {
        trainingData.setClass(attrClass)
        testData.setClass(attrClass)
    }

    override fun buildInstance(taskSample: TaskSample): Instance {
        val instance = DenseInstance(numAttributes())
        instance.setValue(attrTaskType, taskSample.taskType.toDouble())
        instance.setValue(attrTaskSize, taskSample.taskSize)
        return instance
    }


    override fun loadModel() {
        println("Loading model from: $modelPath")
        var file: File? = File(modelPath)
        if (file?.exists() == false) {
            println("Model: $modelPath not found")
            file = downloadModel()
        }
        if (file?.exists() == true) {
            regressor = SerializationHelper.read(FileInputStream(modelPath)) as J48?
        }
    }
}