package pl.edu.agh.sm.ml

import pl.edu.agh.sm.data.TaskSample
import weka.classifiers.AbstractClassifier
import weka.core.Attribute
import weka.core.Instance
import weka.core.Instances
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

abstract class WekaConfig(private val serverPath: String,
                          private val modelDir: String,
                          private val modelType: String,
                          private val deviceType: String?) {

    private val boolValues = ArrayList<String>()
    private val connectionTypeValues = ArrayList<String>()
    private val classValues = ArrayList<String>()

    val attrTaskSize = Attribute("attrTaskSize")
    val attrTaskType = Attribute("attrTaskType")

    val attrIsCharging: Attribute
    val attrBatteryLevel = Attribute("attrBatteryLevel")
    val attrConnectionType: Attribute

    val attrYearDay = Attribute("attrYearDay")
    val attrWeekDay = Attribute("attrWeekDay")
    val attrCurrentTime = Attribute("attrCurrentTime")

    val attrClass: Attribute

    abstract val trainingData: Instances
    abstract val testData: Instances
    abstract val modelPath: String

    var regressor: AbstractClassifier? = null

    init {
        boolValues.add("true")
        boolValues.add("false")
        attrIsCharging = Attribute("attrIsCharging", boolValues)

        connectionTypeValues.add("?")
        connectionTypeValues.add("None")
        connectionTypeValues.add("Wi-Fi")
        connectionTypeValues.add("EDGE")
        connectionTypeValues.add("HSPA")
        connectionTypeValues.add("HSPAP")
        connectionTypeValues.add("LTE")
        attrConnectionType = Attribute("attrConnectionType", connectionTypeValues)

        classValues.add("V_SHORT")
        classValues.add("SHORT")
        classValues.add("MEDIUM")
        classValues.add("LONG")
        classValues.add("V_LONG")
        attrClass = Attribute("class", classValues)
    }

    abstract fun buildInstance(taskSample: TaskSample): Instance

    private fun addTestInstance(instance: Instance) {
        instance.setDataset(testData)
        testData.add(instance)
    }

    fun addTestTaskSample(taskSample: TaskSample) = addTestInstance(buildInstance(taskSample))

    fun numAttributes(): Int = trainingData.numAttributes()

    abstract fun loadModel()

    fun downloadModel(): File? {

        val outputFile = File(modelPath)

        val urlPath = "$serverPath/api/model/$modelType/${deviceType ?: ""}"

        var serverResponseCode = 0

        try {
            println("Downloading model from: $urlPath")

            val url = URL(urlPath)

            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.doOutput = false
            connection.useCaches = false
            connection.requestMethod = "GET"
            connection.setRequestProperty("Connection", "Keep-Alive")

            writeStreamToFile(connection.inputStream, outputFile)

            serverResponseCode = connection.responseCode
        } catch (e: IOException) {
        }

        if (serverResponseCode == 200) {
            println("Model successfully saved in $modelPath")
            return outputFile
        }
        println("Download from $urlPath failed, code: $serverResponseCode")
        return null
    }

    private fun writeStreamToFile(inputStream: InputStream, outputFile: File) {
        val dataInputStream = BufferedInputStream(inputStream)
        val fileOutputStream = FileOutputStream(outputFile)

        rewriteStream(dataInputStream, fileOutputStream)

        dataInputStream.close()
        fileOutputStream.flush()
        fileOutputStream.close()
    }

    private fun rewriteStream(inputStream: InputStream, outputStream: OutputStream) {
        var bytesRead: Int
        val buffer = ByteArray(1 * 1024 * 1024)

        bytesRead = inputStream.read(buffer)
        while (bytesRead != -1) {
            outputStream.write(buffer, 0, bytesRead)
            bytesRead = inputStream.read(buffer)
        }
    }
}