package pl.edu.agh.sm

import android.os.Build
import android.os.Handler
import com.google.gson.GsonBuilder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jcodec.api.transcode.Options
import org.jcodec.api.transcode.SinkImpl
import org.jcodec.api.transcode.SourceImpl
import org.jcodec.api.transcode.Transcoder
import org.jcodec.common.Codec
import org.jcodec.common.Format
import org.jcodec.common.JCodecUtil
import pl.edu.agh.sm.data.TaskSample
import pl.edu.agh.sm.ml.PredictionModel
import pl.edu.agh.sm.ml.WekaConfigLocal
import pl.edu.agh.sm.ml.WekaConfigRemote
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import kotlin.random.Random
import kotlin.system.measureTimeMillis


class TaskService {
    private val serverPath = "http://38ba00c9.ngrok.io"
    private val inPath = "/storage/emulated/0/Movies"
    private val outPath = "/storage/emulated/0/Movies/out"

    private val wekaConfigLocal = WekaConfigLocal(serverPath, inPath, "time", Build.MODEL)
    private val wekaConfigRemote = WekaConfigRemote(serverPath, inPath, "time", null)
    private val predictionModelLocal = PredictionModel(wekaConfigLocal)
    private val predictionModelRemote = PredictionModel(wekaConfigRemote)

    init {
        val outDir = File(outPath)
        if (!outDir.exists()) outDir.mkdir()

        GlobalScope.launch {
            wekaConfigLocal.downloadModel()
            wekaConfigRemote.downloadModel()
        }
    }


    fun convert(fileName: String, taskSample: TaskSample, messageHandler: Handler) {
        println("Starting task: $taskSample")
        println("File: $fileName")

        val inputFile = File("$inPath/$fileName")
        val outputFile = File("$outPath/$fileName")
        taskSample.taskSize = inputFile.length().toDouble()

        val predictedLocalTime = predictionModelLocal.predict(taskSample)
        val predictedRemoteTime = predictionModelRemote.predict(taskSample)

        println("Predicted local timeRange: " + predictedLocalTime.toString())
        println("Predicted remote timeRange: " + predictedRemoteTime.toString())

        var executionTime = measureTimeMillis {


            if (taskSample.connectionType == "None") {
                taskSample.remote = false
            } else if (predictedLocalTime != null && predictedRemoteTime != null && predictedLocalTime != predictedRemoteTime) {
                taskSample.remote = predictedLocalTime > predictedRemoteTime
                if (Random.nextDouble() < 0.25) {
                    println("Experiment")
                    taskSample.remote = Random.nextBoolean()
                }
            } else taskSample.remote = Random.nextBoolean()

            taskSample.remote = Random.nextBoolean()

            taskSample.remote?.let {
                if (it) {
                    messageHandler.obtainMessage(0,
                        "Converting $fileName remotely\n" +
                                "Local:  $predictedLocalTime\n" +
                                "Remote: $predictedRemoteTime").sendToTarget()
                    uploadFileRemote(inputFile, outputFile)
                } else {
                    messageHandler.obtainMessage(0,
                        "Converting $fileName locally\n" +
                                "Local:  $predictedLocalTime\n" +
                                "Remote: $predictedRemoteTime").sendToTarget()
                    uploadFileLocal(inputFile, outputFile)
                }
            }
        }

        if(taskSample.remote == true) executionTime += 70000 + Random.nextInt(10000)

        taskSample.time = executionTime.toDouble()
        taskSample.timeRange = PredictionModel.discretise(taskSample.time).toString()
        postTaskSample(taskSample)
    }

    private fun uploadFileRemote(inputFile: File, outputFile: File): Int {

        val urlPath = "$serverPath/api/convert"

        var serverResponseCode = 0

        val lineEnd = "\r\n"
        val twoHyphens = "--"
        val boundary = "----"

        if (!inputFile.isFile) {
            return 0
        } else {
            try {
                val fileInputStream = FileInputStream(inputFile)

                println("Executing remotely")

                val url = URL(urlPath)

                val connection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.doOutput = true
                connection.useCaches = false
                connection.requestMethod = "POST"
                connection.setRequestProperty("Connection", "Keep-Alive")
                connection.setRequestProperty("content-type", "multipart/form-data;boundary=$boundary")

                val dataOutputStream = DataOutputStream(connection.outputStream)

                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd)
                dataOutputStream.writeBytes("Content-Disposition: form-data; ")
                dataOutputStream.writeBytes("name=\"file\";")
                dataOutputStream.writeBytes("filename=\"${inputFile.absolutePath}\"$lineEnd")

                dataOutputStream.writeBytes(lineEnd)

                rewriteStream(fileInputStream, dataOutputStream)

                dataOutputStream.writeBytes(lineEnd)
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)

                println("File uploaded")

                writeStreamToFile(connection.inputStream, outputFile)

                serverResponseCode = connection.responseCode
                if (serverResponseCode == 200) {
                    println("Remote execution finished")
                } else {
                    println("Remote execution failed: $serverResponseCode")
                }

                fileInputStream.close()
                dataOutputStream.flush()
                dataOutputStream.close()

            } catch (e: IOException) {
                e.printStackTrace()
            }

            return serverResponseCode
        }
    }

    private fun uploadFileLocal(inputFile: File, outputFile: File): Int {

        println("Executing locally")

        val format = JCodecUtil.detectFormat(inputFile)
        val source = SourceImpl(
            inputFile.absolutePath, format,
            JavaCompat.tuple3(0, 0, Codec.H264),
            JavaCompat.tuple3(0, 0, Codec.AAC)
        )
        val sink = SinkImpl(outputFile.absolutePath, Format.MOV, Codec.H264, Codec.AAC)
        sink.setOption(Options.PROFILE, "PROXY")

        val builder = Transcoder.newTranscoder()
        builder.addSource(source)
        builder.addSink(sink)
        builder.setAudioMapping(0, 0, true)
        builder.setVideoMapping(0, 0, false)

        val transcoder = builder.create()
        transcoder.transcode()

        println("Local execution finished")

        return 200
    }

    private fun postTaskSample(taskSample: TaskSample): Int {
        val urlPath = "$serverPath/api/task"
        var serverResponseCode = 0

        try {
            println("postTaskSample")

            val gson = GsonBuilder().create()
            val url = URL(urlPath)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.doOutput = true
            connection.useCaches = false
            connection.requestMethod = "POST"
            connection.setRequestProperty("Connection", "Keep-Alive")
            connection.setRequestProperty("content-type", "application/json")

            val dataOutputStream = DataOutputStream(connection.outputStream)

            println(gson.toJson(taskSample))
            dataOutputStream.writeBytes(gson.toJson(taskSample))

            serverResponseCode = connection.responseCode
            if (serverResponseCode == 200) {
                println("Sample sent to server")
            } else {
                println(serverResponseCode)
            }

            dataOutputStream.flush()
            dataOutputStream.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return serverResponseCode
    }

    private fun writeStreamToFile(inputStream: InputStream, outputFile: File) {
        val dataInputStream = BufferedInputStream(inputStream)
        val fileOutputStream = FileOutputStream(outputFile)

        rewriteStream(dataInputStream, fileOutputStream)

        dataInputStream.close()
        fileOutputStream.flush()
        fileOutputStream.close()
    }

    private fun rewriteStream(inputStream: InputStream, outputStream: OutputStream) {
        var bytesRead: Int
        val buffer = ByteArray(1 * 1024 * 1024)

        bytesRead = inputStream.read(buffer)
        while (bytesRead != -1) {
            outputStream.write(buffer, 0, bytesRead)
            bytesRead = inputStream.read(buffer)
        }
    }

    fun availableFiles(): List<String> {
        return File(inPath).listFiles { _, name -> name.toLowerCase().endsWith(".mp4") }.map { it.name }
    }
}
